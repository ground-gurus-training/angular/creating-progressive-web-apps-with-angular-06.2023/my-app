import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HellowerService {
  getGreet(yourName: string): Observable<string> {
    return of(`Welcome ${yourName}!`);
  }
}
