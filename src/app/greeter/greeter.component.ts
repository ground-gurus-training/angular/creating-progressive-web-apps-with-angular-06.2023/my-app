import { Component } from '@angular/core';
import { HellowerService } from '../hellower.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-greeter',
  templateUrl: './greeter.component.html',
  styleUrls: ['./greeter.component.css'],
})
export class GreeterComponent {
  yourName = '';
  message$!: Observable<string>;

  constructor(private hellowerService: HellowerService) {}

  greetMe() {
    this.message$ = this.hellowerService.getGreet(this.yourName);
    // this.hellowerService.getGreet(this.yourName).subscribe((yourName) => {
    //   this.message = yourName;
    // });
  }
}
