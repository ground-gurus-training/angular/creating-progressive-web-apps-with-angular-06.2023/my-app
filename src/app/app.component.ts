import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'my-app';
  message: string = 'Hello po sa lahat';
  age: number = 30.5;
  address: any = {
    address1: 'Purok 3, Dyan lang sa tabi',
    address2: 'Quezon St.',
    postalCode: '1235',
    city: 'Quezon',
    province: 'Metro Manila',
  };
  guesses: number[] = [23, 49, 98, 45, 37, 32];

  greetMe() {
    console.log('Kumusta?');
  }
}
